# Style Guide

We use the Google C++ Style Guide that can be found at https://google.github.io/styleguide/cppguide.html

# Testing

We use the test framework [catch2](https://github.com/catchorg/Catch2) for writing and running the test cases.

# Documentation

We use [Doxygen](http://www.doxygen.org/) for writing and generating the documentation.
