# Factor Oracle

C++ implementation of the Factor Oracle structure.

# Dependencies

- `cmake`

# Build

1. `mkdir build`
2. `cd build`
3. `cmake ..`
4. `make`

# Generate Documentation

To generate the documentation you need to replace the step 3 above by `cmake .. -DBUILD_DOCS=ON`.
